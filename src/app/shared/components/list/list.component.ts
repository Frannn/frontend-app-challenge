import { ChangeDetectionStrategy, Component, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListComponent<T> implements OnInit{

  @Input() items!: Array<T>;

  @Input() itemTemplate: TemplateRef<any> | undefined;

  @Input() columns = 1;

  ngOnInit(): void {
    this.columns -= 1
  }

  getColumnArray(): Array<number> {
    return Array(Math.ceil(this.items.length/this.columns));
  }

}
