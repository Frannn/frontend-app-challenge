import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list.component';
import {MatGridListModule} from '@angular/material/grid-list';

@NgModule({
  declarations: [
    ListComponent
  ],
  imports: [
    CommonModule,
    MatGridListModule
  ],
  providers: [],
  exports: [ListComponent],
})
export class ListModule { }
