import { GraphicCardModel } from "./graphic-card.model";

export interface FilterModel{
  isActive: boolean,
  filterString: string,
  filterType: string,
}

export interface GraphicCardsState {
  loading: boolean,
  scrollPosition: number;
  items: ReadonlyArray<GraphicCardModel>,
  filter: FilterModel;
  filteredItems: ReadonlyArray<GraphicCardModel>,
}
