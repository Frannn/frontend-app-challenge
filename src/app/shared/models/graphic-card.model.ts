export interface GraphicCardResponseModel {
  data: GraphicCardModel[];
}

export interface GraphicCardModel {
  image: string;
  manufacturer: string;
  model: string;
  name: string;
  price: number;
}
