import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, ParamMap, Params, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable, filter, map, take, Subject, takeUntil } from 'rxjs';
import { GraphicsCardService } from 'src/app/services/graphic-cards/graphic-cards.service';
import { GraphicCardModel } from 'src/app/shared/models/graphic-card.model';
import { AppState } from 'src/app/state/app.state';
import { selectGraphicCardDetailById, selectGraphicCardScrollPosition } from 'src/app/state/selectors/graphic-cards.selector';

@Component({
  selector: 'app-graphic-card-details',
  templateUrl: './graphic-card-details.component.html',
  styleUrls: ['./graphic-card-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GraphicCardDetailsComponent {
  id: string | null;

  graphicCard: Observable<GraphicCardModel>;

  destroy$: Subject<boolean> = new Subject();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>,
  ) {
    this.id = this.route.snapshot.paramMap.get('id') || null;
    this.graphicCard = this.store.select(selectGraphicCardDetailById(this.id ? parseInt(this.id) : 0)).pipe(takeUntil(this.destroy$));
  }

  ngOnDestroy() {
    this.destroy$.next(true);
  }

  back() {
    this.store.select(selectGraphicCardScrollPosition).pipe(takeUntil(this.destroy$)).subscribe(scrollPosition => {
      this.router.navigate([''], { queryParams: { scrollTo: scrollPosition } });
    })
  }

}
