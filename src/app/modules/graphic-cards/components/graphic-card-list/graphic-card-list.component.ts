import { ChangeDetectionStrategy, Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { Subject, Observable, filter, takeUntil, fromEvent, take, combineLatest, map, switchMap, mergeMap, tap } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { isScrollingGraphicCardList, loadGraphicCards, loadGraphicCardsNextPage } from 'src/app/state/actions/graphic-cards.actions';
import { selectGraphicCardsBySearch, selectGraphicCardsList, selectGraphicCardsListLoading } from 'src/app/state/selectors/graphic-cards.selector';
import { AppState } from 'src/app/state/app.state';
import { ViewportScroller } from '@angular/common';

@Component({
  selector: 'app-graphic-card-list',
  templateUrl: './graphic-card-list.component.html',
  styleUrls: ['./graphic-card-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GraphicCardListComponent implements OnInit, OnDestroy {

  loading$: Observable<boolean> = new Observable();
  items$: Observable<any> = new Observable();
  destroy$ = new Subject();

  constructor(
    private router: Router,
    private store: Store<AppState>,
    private zone: NgZone,
    private route: ActivatedRoute,
    private viewportScroller: ViewportScroller,
  ) { }

  ngOnInit(): void {
    this.loadData();
    this.loadParams();
    this.listenToScrollPosition();
  }

  ngOnDestroy() {
    this.destroy$.next({});
  }

  itemClickHandler(id: number) {
    this.router.navigate([id]);
  }

  loadNextPage() {
    this.store.dispatch(loadGraphicCardsNextPage());
  }

  private loadData() {
    this.store.dispatch(loadGraphicCards());
    this.loading$ = this.store.select(selectGraphicCardsListLoading);
    this.items$ =
      combineLatest([
        this.store.select(selectGraphicCardsList),
        this.store.select(selectGraphicCardsBySearch)])
        .pipe(map(res => {
          const [originalList, filteredList] = res;
          console.log(originalList);
          if (filteredList.length) return filteredList
          else return originalList;
        })
        );
  }



  private loadParams() {
    const scrollParam$ = this.route.queryParams.pipe(
      take(1),
      filter((params: Params) => params['scrollTo']), tap(console.log)
    );
    this.loading$.pipe(
      filter(l => !l),
      switchMap(() => scrollParam$),
      take(1),
    ).subscribe((params: Params) => {
      setTimeout(() => {
        this.viewportScroller.scrollToPosition(params['scrollTo'])
        window.scrollTo({ top: params['scrollTo'] });
      }, 0);
    });
  }


  private listenToScrollPosition() {
    this.zone.runOutsideAngular(() => {
      fromEvent(window, 'scroll').pipe(
        tap(() => this.store.dispatch(isScrollingGraphicCardList({ position: window.scrollY }))),
        mergeMap(() => this.store.select(selectGraphicCardsListLoading).pipe(take(1))),
        filter(isLoading => !isLoading),
        filter(() => (window.scrollY + window.document.body.offsetHeight >= window.document.body.scrollHeight)),
        takeUntil(this.destroy$),
      ).subscribe(() => {
        this.zone.run(() => {
          this.loadNextPage();
        });
      });
    });
  }

}
