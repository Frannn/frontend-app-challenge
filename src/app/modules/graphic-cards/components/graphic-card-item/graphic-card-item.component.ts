import { ChangeDetectionStrategy, Component, HostBinding, Input, OnInit } from '@angular/core';
import { GraphicCardModel } from 'src/app/shared/models/graphic-card.model';

@Component({
  selector: 'app-graphic-card-item',
  templateUrl: './graphic-card-item.component.html',
  styleUrls: ['./graphic-card-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GraphicCardItemComponent {

  @HostBinding('class.graphic-card-item') baseClass = true;

  @Input() item!: GraphicCardModel;

}
