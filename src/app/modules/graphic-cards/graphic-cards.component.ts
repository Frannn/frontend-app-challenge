import { loadFilteredGraphicCardsListSuccess } from './../../state/actions/graphic-cards.actions';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { loadFilteredGraphicCardsList } from 'src/app/state/actions/graphic-cards.actions';
import { AppState } from 'src/app/state/app.state';
import { selectGraphicCardsBySearch, selectGraphicCardsList } from 'src/app/state/selectors/graphic-cards.selector';

@Component({
  selector: 'app-graphic-cards',
  templateUrl: './graphic-cards.component.html',
  styleUrls: ['./graphic-cards.component.scss']
})
export class GraphicCardsComponent {

  searchBarForm = new FormGroup({
    filterType: new FormControl(),
    filterString: new FormControl(),
  });

  searchString = '';

  constructor(private store: Store<AppState>) { }


  search() {
    this.store.select(selectGraphicCardsList);
  }

  onSubmit() {
    const filterType = this.searchBarForm.get<string>('filterType')?.value;
    const filterString = this.searchBarForm.get<string>('filterString')?.value;
    this.store.dispatch(loadFilteredGraphicCardsListSuccess({filter: {filterType, filterString, isActive:true}}));
  }

}
