import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GraphicCardsComponent } from './graphic-cards.component';
import { GraphicCardListComponent } from './components/graphic-card-list/graphic-card-list.component';
import { GraphicCardDetailsComponent } from './components/graphic-card-details/graphic-card-details.component';

const routes: Routes = [
  {
    path: '',
    component: GraphicCardsComponent,
    children: [
      {
        path: '',
        component: GraphicCardListComponent,
      }]
  },
  {
    path: ':id',
    component: GraphicCardDetailsComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GraphicCardsRoutingModule { }
