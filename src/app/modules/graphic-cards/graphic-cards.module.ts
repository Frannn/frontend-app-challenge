import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GraphicCardsRoutingModule } from './graphic-cards-routing.module';
import { GraphicCardsComponent } from './graphic-cards.component';
import { GraphicsCardService } from 'src/app/services/graphic-cards/graphic-cards.service';
import { GraphicCardListComponent } from './components/graphic-card-list/graphic-card-list.component';
import { GraphicCardItemComponent } from './components/graphic-card-item/graphic-card-item.component';
import { ListModule } from 'src/app/shared/components/list/list.module';
import { RouterModule } from '@angular/router';
import { GraphicCardDetailsComponent } from './components/graphic-card-details/graphic-card-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    GraphicCardsComponent,
    GraphicCardListComponent,
    GraphicCardItemComponent,
    GraphicCardDetailsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    GraphicCardsRoutingModule,
    ListModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
  ],
  providers: [GraphicsCardService],
})
export class GraphicCardsModule { }
