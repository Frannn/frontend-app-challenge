import { GraphicCardsState } from 'src/app/shared/models/graphic-card.state';
import { AppState } from './../app.state';
import { createSelector } from "@ngrx/store";


export const selectGraphicCardsFeature = (state: AppState) => state.graphicCards;

export const selectGraphicCardsList = createSelector(
  selectGraphicCardsFeature,
  (state: GraphicCardsState) => state.items
);

export const selectGraphicCardsListLoading = createSelector(
  selectGraphicCardsFeature,
  (state: GraphicCardsState) => state.loading
);

export const selectGraphicCardDetailById = (id:number) => createSelector(
  selectGraphicCardsFeature,
  (state: GraphicCardsState) => state.items[id]
);

export const selectGraphicCardScrollPosition = createSelector(
  selectGraphicCardsFeature,
  (state: GraphicCardsState) => state.scrollPosition
);

export const selectGraphicCardsBySearch = createSelector(
  selectGraphicCardsFeature,
  (state: GraphicCardsState) => {
    return state.filteredItems;
  }
);
