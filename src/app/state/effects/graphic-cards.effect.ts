import { GraphicsCardService } from 'src/app/services/graphic-cards/graphic-cards.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';


@Injectable()
export class GraphicCardsEffects {

  constructor(
    private action$: Actions,
    private graphicsCardService: GraphicsCardService,
  ) { }

  loadItems$ = createEffect(() => this.action$.pipe(
    ofType('[Graphic Cards List] Load Items'),
    mergeMap( () => this.graphicsCardService.getGraphicCards()
    .pipe(
      map(res => ({type:'[Graphic Cards List] Load Success', items: res}))
    ))
  ));

  loadItemsNextPage$ = createEffect(() => this.action$.pipe(
    ofType('[Graphic Cards List] Load Next Page Items'),
    mergeMap( () => this.graphicsCardService.getGraphicCardsNextPage()
    .pipe(
      map(res => ({type:'[Graphic Cards List] Load Next Page Success', newItems: res}))
    ))
  ));

}
