import { ActionReducerMap } from "@ngrx/store";
import { GraphicCardsState } from "../shared/models/graphic-card.state";
import { graphicCardsReducer } from "./reducers/graphic-cards.reducer";

export interface AppState {
  graphicCards: GraphicCardsState,
}


export const ROOT_REDUCERS: ActionReducerMap<AppState> = {
  graphicCards: graphicCardsReducer,
};
