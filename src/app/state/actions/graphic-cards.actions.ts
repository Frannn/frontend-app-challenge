import { createAction, props } from "@ngrx/store";
import { GraphicCardModel } from "src/app/shared/models/graphic-card.model";
import { FilterModel } from "src/app/shared/models/graphic-card.state";


export const loadGraphicCards = createAction(
  '[Graphic Cards List] Load Items'
)

export const loadedGraphicCards = createAction(
  '[Graphic Cards List] Load Success',
  props<{items: GraphicCardModel[]}>(),
)

export const loadGraphicCardsNextPage = createAction(
  '[Graphic Cards List] Load Next Page Items'
)

export const loadedGraphicCardsNextPage = createAction(
  '[Graphic Cards List] Load Next Page Success',
  props<{newItems: GraphicCardModel[]}>(),
)

export const isScrollingGraphicCardList = createAction(
  '[Graphic Cards List] Scrolling Graphic Card List',
  props<{position: number}>(),
)

export const loadFilteredGraphicCardsList = createAction(
  '[Graphic Cards List] Load Filtered Graphic Card List',
)

export const loadFilteredGraphicCardsListSuccess = createAction(
  '[Graphic Cards List] Load Filtered Graphic Card List Success',
  props<{filter: FilterModel}>(),
)
