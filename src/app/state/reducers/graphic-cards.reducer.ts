import { createReducer, on } from "@ngrx/store";
import { loadGraphicCards, loadGraphicCardsNextPage, loadedGraphicCards, loadedGraphicCardsNextPage, isScrollingGraphicCardList, loadFilteredGraphicCardsList, loadFilteredGraphicCardsListSuccess } from "../actions/graphic-cards.actions";
import { GraphicCardsState } from "src/app/shared/models/graphic-card.state";


export const initialState: GraphicCardsState = {
  loading: false,
  scrollPosition: 0,
  filter: {
    isActive: false,
    filterString: '',
    filterType: '',
  },
  items: [],
  filteredItems: [],
};


export const graphicCardsReducer = createReducer(
  initialState,
  on(loadGraphicCards, (state) => {
    return { ...state, loading: true };
  }),
  on(loadedGraphicCards, (state, { items }) => {
    return { ...state, loading: false, items };
  }),
  on(loadGraphicCardsNextPage, (state) => {
    return { ...state, loading: true };
  }),
  on(loadedGraphicCardsNextPage, (state, { newItems }) => {
    return { ...state, loading: false, items: [...state.items, ...newItems] }
  }),
  on(isScrollingGraphicCardList, (state, { position }) => {
    return { ...state, scrollPosition: position };
  }),
  on(loadFilteredGraphicCardsListSuccess, (state, {filter}) => {
    const filteredItems = state.items.filter(i => {
      const property = () => {
        switch (filter.filterType) {
          case 'brand':
            return i.manufacturer;
          case 'name':
            return i.name;
          }
          return '';
      }
      return property()?.toLowerCase().includes(filter.filterString) || false;
    });
    return {...state, loading: false, items: [...filteredItems], ...filter};
  }),
)
