# Graphics card catalog app

Your task is to write a very simple graphics card catalog app.

### Exercise 1. Write a simple REST API in whatever language you're most comfortable (NodeJS, Rails, Java…) that…

- Has 1 endpoint /graphics-cards
- Returns a couple of graphics cards like Asus ROG Strix GeForce RTX 3090, MSI AMD Radeon RX 6900 XT, etc. With
some simple attributes like product image (just take some off of google images), name, manufacturer,
model, assembler, price, etc.
The data can all be mocked, no need for a persistence layer.

### Exercise 2. Write an Angular/React app that displays the graphics cards from the API

- Create a GraphicsCardListContainer component that shows the graphics cards at /graphics-cards
- Create a GraphicsCardDetail component that shows a few more details about the graphics card
when the user selects it at /graphics-cards/:id
- Graphics card list must support infinite scroll, requesting to API and adding more elements to the list as we go down.
- Display a spinner or skeleton placeholder component while the API requests are ongoing
- Create a searchbar that allows to filter the graphics cards by name or brand.
- Make it look decent. No need for super sophisticated design, but at a minimum,
make it somewhat responsive so that it doesn’t look terrible on a mobile phone.

#### Code Requirements:
- Handle async behaviours with Rxjs observables.
- Clean code good practices (single responsibility, abstraction layers, etc).
- Add to tsconfig.json this compiler options, to ensure strong typing:
`
"compilerOptions": {
        "strict": true,
        "noImplicitReturns": true}
`
- Use only core libraries (Angular/React, Ngrx/Redux, Rxjs, etc), don't add unnecessary libraries to package.json.

##### In case you choose to do it with Angular:
- Use OnPush Change Detection strategy on components.
- Use NgRx for state management.

##### ... or with React:
- Use Typescript.
- Use React Hooks.
- Use Redux for state management.

### Bonus Points

- Dockerizing the app.
- Realistic unit/end-to-end tests.
- Linters/code format.
- Scroll state could be defined via queryParams.
- Search filter state could be defined via queryParams.

### Push the code to a public github repo with a README.md that explains how to run/test/deploy with Exercise 1 and 2 (API & Frontend app)
